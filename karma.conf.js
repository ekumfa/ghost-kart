'use strict'

const CAPTURE_TIMEOUT = 5 * 60 * 1000
const NO_ACTIVITY_TIMEOUT = 60 * 1000

let webpackCfg = require('./webpack.config')

module.exports = function (config) {
  config.set({
    browserNoActivityTimeout: NO_ACTIVITY_TIMEOUT,
    browsers: ['Chrome'],
    captureTimeout: CAPTURE_TIMEOUT,
    files: ['test/index.js'],
    frameworks: ['mocha'],
    logLevel: config.LOG_DEBUG,
    preprocessors: {
      'test/**/*.js': ['webpack', 'sourcemap']
    },
    reporters: 'dots',
    singleRun: false,
    webpack: webpackCfg,
    webpackServer: {
      noInfo: true
    }
  })
}
