'use strict';

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var uuid = require('node-uuid');
var isFunction = require('lodash/isFunction');
var defaultsDeep = require('lodash/defaultsDeep');
var RemediationUtils = require('./RemediationUtils');
var findDOMNode = require('react/lib/findDOMNode');
var warnings = require('./warnings');

var Recorder = function () {
  function Recorder() {
    var config = arguments.length <= 0 || arguments[0] === undefined ? {} : arguments[0];

    _classCallCheck(this, Recorder);

    this.id = uuid.v1();
    this.rootEl = config.rootEl;
    this.actions = [];
    this.isRecording = false;
    this.config = config;
  }

  _createClass(Recorder, [{
    key: 'addAction',


    /**
     * This method adds a user interaction to the record.
     * @param {object} data Arbitrary metadata that describes the event
     * @returns {void}
     */
    value: function addAction(data) {
      var rootDomEl = findDOMNode(this.rootEl);

      if (!data.component && typeof data.skipMapping === 'undefined') {
        warnings.actionMissingImportantParts(data);
      }

      data = defaultsDeep(data || {}, {
        type: '',
        event: {},
        component: {},
        meta: {},
        dom: {
          id: rootDomEl.id,
          width: rootDomEl.offsetWidth,
          height: rootDomEl.offsetHeight
        }
      });

      // Do this now because React's SyntheticEvent is reused.
      data.event = RemediationUtils.convertEvent(data.event, {
        root: this.rootEl
      });

      var insMode = data.insertion || 'append';
      var currLen = this.actions.length;

      if (isFunction(insMode)) {
        insMode = insMode(this.actions[currLen - 1], data);
      }

      switch (insMode) {
        case 'negate':
          // remove last action
          this.actions.splice(-1, 1);
          break;
        case 'replace':
          // remove last action and replace with this one
          this.actions.splice(-1, 1, data);
          break;
        default:
          // append current action
          this.actions.push(data);
      }
    }
  }, {
    key: 'pause',
    value: function pause() {
      this.isRecording = false;
    }
  }, {
    key: 'record',
    value: function record() {
      this.isRecording = true;
    }
  }, {
    key: 'sequence',
    get: function get() {
      return this.actions;
    }
  }]);

  return Recorder;
}();

module.exports = Recorder;