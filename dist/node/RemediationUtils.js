/* eslint no-undefined: 0 */
'use strict';

var includes = require('lodash/includes');
var React = require('react');
var findDOMNode = React.findDOMNode;

var IGNORED_EVENT_PROPS = ['applicationCache', 'bubbles', 'cancelable', 'clientInformation', 'chrome', 'console', 'crypto', 'defaultPrevented', 'dispatchConfig', 'dispatchMarker', '_dispatchIDs', '_dispatchListeners', 'external', 'frames', 'isTrusted', 'nativeEvent', 'navigator', 'path', 'parent', 'performance', 'runtime', 'screen', 'self', 'sourceCapabilities', 'styleMedia', 'target', 'top', 'view', 'webkitIndexedDB', 'webstore', 'window'];

var RemediationUtils = void 0;

function processProp(config) {
  return function (key, val) {
    var retVal = void 0;

    switch (true) {
      case includes(IGNORED_EVENT_PROPS, key):
        retVal = undefined;
        break;
      case typeof val === 'function':
        retVal = undefined;
        break;
      case val instanceof Window:
        retVal = 'window';
        break;
      case val instanceof HTMLElement:
        retVal = RemediationUtils.getDOMSelector(val, config.root);
        break;
      default:
        retVal = val;
    }

    return retVal;
  };
}

function processEvent(evt, func) {
  var out = {};
  for (var key in evt) {
    // eslint-disable-line
    var val = func(key, evt[key]);
    if (val !== undefined) {
      out[key] = val;
    }
  }
  return out;
}

function processSyntheticEvent(evt, func) {
  return JSON.parse(JSON.stringify(evt, func));
}

RemediationUtils = {
  red: function red(str) {
    return '<span style="color: red;" title="Proper label not found">' + str + '</span>';
  },
  getAncestorRealms: function getAncestorRealms(component) {
    var realms = [];
    while (component && component.context && component.context.realm) {
      realms.push(component = component.context.realm);
    }
    return realms.reverse();
  },


  /*
   * config = {
   *   root: [The root simulation ReactElement]
   * }
   */
  convertEvent: function convertEvent(evt) {
    var config = arguments.length <= 1 || arguments[1] === undefined ? {} : arguments[1];

    var func = processProp(config);
    var out = void 0;
    if (evt instanceof Event) {
      out = processEvent(evt, func);
    } else {
      out = processSyntheticEvent(evt, func);
    }
    return out;
  },
  getDOMSelector: function getDOMSelector(node, rootEl) {
    var parts = [];
    var retVal = '';
    var par = node;
    var nodeInd = void 0;
    var foundReactID = false;
    var rootNode = findDOMNode(rootEl);
    while (par && par !== rootNode && !foundReactID) {
      if (par.dataset && par.dataset.reactid) {
        parts.push(par);
        foundReactID = true;
      }
      par = par.parentNode;
    }

    retVal = parts.reverse().map(function (el) {
      return '[data-reactid="' + el.dataset.reactid + '"]';
    }).join(' ');

    return retVal;
  }
};

module.exports = RemediationUtils;