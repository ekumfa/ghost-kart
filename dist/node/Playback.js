'use strict';

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var uuid = require('node-uuid');
var isEmpty = require('lodash/isEmpty');

var _require = require('react/lib/ReactTestUtils');

var Simulate = _require.Simulate;


var EventMap = require('./playback/DOMToReactTypeMap');

var DEFAULT_ROOT_EL_ID = 'ArubaSimulation';
var PLAY_MIN_SPEED = 200;
var DEFAULT_ACTION_DELAY = 10;

function generateError(action, msg) {
  var err = new Error(msg);
  err.details = { action: action };
  return err;
}

var Player = function () {
  function Player() {
    var actions = arguments.length <= 0 || arguments[0] === undefined ? [] : arguments[0];
    var config = arguments.length <= 1 || arguments[1] === undefined ? {} : arguments[1];

    _classCallCheck(this, Player);

    this.actions = actions;
    this.config = config;
    this.currentIndex = 0;
  }

  _createClass(Player, [{
    key: 'playCurrentAction',
    value: function playCurrentAction(done) {
      var action = this.actions[this.currentIndex];
      var genErr = generateError.bind(null, action);

      if (action._meta) {
        // eslint-disable-line no-underscore-dangle
        var data = action._meta.dom; // eslint-disable-line no-underscore-dangle
        var rootEl = document.getElementById(data.id || DEFAULT_ROOT_EL_ID);
        rootEl.style.width = data.width + 'px';
        rootEl.style.height = data.height + 'px';
        try {
          var ev = new Event('resize');
          window.dispatchEvent(ev);
        } catch (err) {
          console.info(err);
          Simulate.resize(window);
        }

        delete action._meta; // eslint-disable-line no-underscore-dangle
      }

      if (!this.config.skipAnimation) {}
      // 2. Move mouse to correct location
      // 3. Play sound


      // 4. Fire synthetic event
      var domSel = action.currentTarget;
      var el = document.querySelector(domSel);
      var type = action.type;

      if (!el) {
        return done(genErr('Could not find element based on given selector: ' + domSel));
      }

      setTimeout(function () {
        var simulatedEvent = Simulate[type] || Simulate[EventMap[type]];
        if (simulatedEvent) {
          simulatedEvent(el, { type: type });
          done();
        } else {
          done(genErr('Could not recreate event of type \'' + type + '\''));
        }
      }, this.config.actionDelay || DEFAULT_ACTION_DELAY);
    }
  }, {
    key: 'play',
    value: function play() {
      var _this = this;

      var actions = this.actions || [];

      if (!isEmpty(actions)) {

        this.playCurrentAction(function (err) {

          if (err) {
            if (_this.config.onComplete) {
              _this.config.onComplete(err);
            }
            return;
          }

          _this.currentIndex = _this.currentIndex + 1;
          if (!_this.isPaused && _this.currentIndex < actions.length) {
            _this.play();
          } else if (_this.currentIndex === actions.length) {
            _this.isPaused = true;
            _this.isComplete = true;
            if (_this.config.onComplete) {
              _this.config.onComplete();
            }
          }
        });
      }
    }
  }]);

  return Player;
}();

var Playback = function () {
  function Playback() {
    var actions = arguments.length <= 0 || arguments[0] === undefined ? [] : arguments[0];
    var config = arguments.length <= 1 || arguments[1] === undefined ? {} : arguments[1];

    _classCallCheck(this, Playback);

    this.id = uuid.v1();
    this.actions = actions;
    this.config = config;
    this.isStarted = false;
    this.isPlaying = false;
    this.player = new Player(this.actions, this.config);
  }

  _createClass(Playback, [{
    key: 'start',
    value: function start() {
      if (!this.isStarted) {
        this.isStarted = true;
        this.player.currentIndex = 0;
      }
      this.resume();
    }
  }, {
    key: 'pause',
    value: function pause() {
      this.isPlaying = false;
      this.player.isPaused = true;
    }
  }, {
    key: 'resume',
    value: function resume() {
      if (!this.isPlaying) {
        this.isPlaying = true;
        this.player.isPaused = false;
        this.player.play();
      }
    }
  }, {
    key: 'isPaused',
    get: function get() {
      return this.isStarted && !this.isPlaying;
    }
  }]);

  return Playback;
}();

module.exports = Playback;