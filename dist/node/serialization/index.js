'use strict';

var JSONSerializer = require('./JSONSerializer');
var TextSerializer = require('./TextSerializer');

module.exports = {
  JSONSerializer: JSONSerializer,
  TextSerializer: TextSerializer
};