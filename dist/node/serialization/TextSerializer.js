'use strict';

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var cloneDeep = require('lodash/cloneDeep');
var defaultsDeep = require('lodash/defaultsDeep');
var difference = require('lodash/difference');

var DEFAULT_SOFT_LIMIT = 500;
var DEFAULT_THRESHOLD = 0.15; // percent

var DEFAULT_MAPPINGS = require('./text/DefaultMappings');
var BUILDERS = require('./text/builders');

var warnings = require('../warnings');
var RemediationUtils = require('../RemediationUtils');

var map = new WeakMap();

function getMapper(action) {
  var defMapClone = cloneDeep(TextSerializer.DEFAULT_MAPPINGS); // eslint-disable-line
  if (action.skipMapping) {
    return defMapClone;
  }

  var component = action.component;
  var constr = component.constructor;
  var mapper = map.get(constr);

  if (!mapper) {
    warnings.componentNotRegistered(constr.name);
    mapper = defMapClone;
  }

  return mapper;
}

function groupActionsByRealms(actions) {
  var groups = [];
  var currentGroup = void 0;

  actions.forEach(function (action) {
    var component = action.component;

    action.realms = RemediationUtils.getAncestorRealms(component);
  });

  function createNewGroupWithAction(action) {
    currentGroup = [action];
    groups.push(currentGroup);
  }

  actions.forEach(function (action, idx) {
    var lastAction = void 0;

    if (idx === 0) {
      // first group
      createNewGroupWithAction(action);
    } else {
      lastAction = actions[idx - 1];

      if (lastAction.realms.length !== action.realms.length) {
        // new group
        createNewGroupWithAction(action);
      } else if (difference(lastAction.realms, action.realms).length > 0) {
        // new group
        createNewGroupWithAction(action);
      } else {
        currentGroup.push(action);
      }
    }
  });

  return groups;
}

function truncateText(text, limit) {
  if (limit === -1 || text.length < limit) {
    return text;
  }

  var lines = text.split('. ').map(function (line) {
    return line.trim();
  }).filter(function (line) {
    return line.length > 0;
  });

  var truncatedText = '';
  var nextText = '';

  for (var i = lines.length; i > 0; i = i - 1) {
    nextText = lines.slice(i - 1).join('. ');
    if (nextText.length < limit) {
      truncatedText = nextText;
    } else {
      break;
    }
  }

  return truncatedText;
}

var TextSerializer = function () {
  _createClass(TextSerializer, null, [{
    key: 'registerComponent',
    value: function registerComponent(cmpClass) {
      var meta = arguments.length <= 1 || arguments[1] === undefined ? {} : arguments[1];

      defaultsDeep(meta, TextSerializer.DEFAULT_MAPPINGS);
      map.set(cmpClass, meta);
    }
  }]);

  function TextSerializer() {
    var actions = arguments.length <= 0 || arguments[0] === undefined ? [] : arguments[0];
    var config = arguments.length <= 1 || arguments[1] === undefined ? {} : arguments[1];

    _classCallCheck(this, TextSerializer);

    actions = actions.filter(function (act) {
      return act.skipText !== true;
    });
    this.actions = actions;
    this.config = config;
  }

  _createClass(TextSerializer, [{
    key: 'serialize',
    value: function serialize() {
      var groups = groupActionsByRealms(this.actions);
      var paragraph = BUILDERS.paragraph(groups, getMapper, this.config.useConnectors || false);

      var softLimit = this.config.softLimit;
      if (typeof softLimit === 'undefined') {
        softLimit = DEFAULT_SOFT_LIMIT;
      }
      var hardLimit = softLimit === -1 ? -1 : Math.floor(softLimit * (1 + DEFAULT_THRESHOLD));

      return truncateText(paragraph, hardLimit);
    }
  }]);

  return TextSerializer;
}();

TextSerializer.DEFAULT_MAPPINGS = DEFAULT_MAPPINGS;


module.exports = TextSerializer;