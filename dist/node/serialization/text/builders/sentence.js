'use strict';

var prepPhrase = require('./prepositionalPhrase');
var predicate = require('./predicate');

var NAME_STR = 'you';
var UPPER_FIRST_NAME_STR = 'You';

var NAME_TAG = '<span class="username">you</span>';

function getNameTag(str) {
  return '<span class="username">' + str + '</span>';
}

module.exports = function (parts, getMapper) {
  var fragments = [];
  var realms = void 0;

  if (parts.length === 0) {
    return fragments;
  }

  // The realms for each part should be the same,
  // so we just neead to take the first set
  realms = parts[0].realms;
  if (realms.length !== 0) {
    // there are prepositions to build
    fragments.push(realms.map(function (cmp) {
      return prepPhrase({ component: cmp }, getMapper);
    }).join(' '));
  }

  fragments = fragments.concat(parts.map(function (action, index) {
    var output = predicate(action, getMapper);
    if (index === 0) {
      output = getNameTag(fragments.length === 0 ? UPPER_FIRST_NAME_STR : NAME_STR) + ' ' + output;
    } else if (parts.length > 2 && index === parts.length - 1) {
      output = 'and ' + output;
    }
    return output;
  }));

  return fragments.join(', ');
};