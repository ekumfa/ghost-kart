'use strict';

var buildDirectObject = require('./directObject');

module.exports = function (action, getMapper) {
  var mapper = getMapper(action);
  var prep = mapper.preposition;
  var complement = buildDirectObject(action, mapper, 'i');
  return prep + ' ' + complement;
};