'use strict';

var isFunction = require('lodash/isFunction');
var isString = require('lodash/isString');
var buildDirectObject = require('./directObject');

module.exports = function (action, getMapper) {
  var predicate = '';
  var event = action.event;
  var component = action.component;

  var mapper = getMapper(action);
  var eventMapper = mapper.events[event.type];

  if (!eventMapper) {
    eventMapper = mapper.events.default;
  }

  if (isString(eventMapper)) {
    predicate = eventMapper;
  } else if (isFunction(eventMapper)) {
    predicate = eventMapper(action);
  } else if (eventMapper.verb) {
    var verb = eventMapper.verb;
    if (isFunction(verb)) {
      verb = verb(action);
    }
    var directObject = buildDirectObject(action, mapper);

    predicate = verb + ' ' + directObject;
  }

  return predicate;
};