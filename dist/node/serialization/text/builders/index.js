'use strict';

var directObject = require('./directObject');
var paragraph = require('./paragraph');
var predicate = require('./predicate');
var prepositionalPhrase = require('./prepositionalPhrase');
var sentence = require('./sentence');

module.exports = {
  directObject: directObject, // Also used for prepositional complement
  paragraph: paragraph,
  predicate: predicate,
  prepositionalPhrase: prepositionalPhrase,
  sentence: sentence
};