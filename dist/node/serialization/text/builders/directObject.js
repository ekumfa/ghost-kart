'use strict';

var isFunction = require('lodash/isFunction');

// Also used for prepositional complement
module.exports = function (action, mapper) {
  var tag = arguments.length <= 2 || arguments[2] === undefined ? 'b' : arguments[2];

  var type = mapper.type;
  var label = mapper.label;

  if (isFunction(type)) {
    type = type(action);
  }

  if (!label) {
    return 'a ' + type;
  }

  if (isFunction(label)) {
    label = label(action);
  }
  return 'the <' + tag + '>' + label + '</' + tag + '> ' + type;
};