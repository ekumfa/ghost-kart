'use strict';

var lowerCase = require('lodash/lowerCase');

var DEBUG_SCOPE = '[ghostKart.TextSerializer.default]';

var getTypeNotSpecifiedText = function getTypeNotSpecifiedText(name) {
  return 'Component type not given for ' + name + '. Using default type converter.\n     To get rid of this warning, go to the file that contains the ' + name + '\n     class, look for the UIUtils.register call and add a \'type\' property.';
};

var getEventNotMappedText = function getEventNotMappedText(event, name) {
  return 'There was no default event mapper for the ' + event.type + '. Using\n     default event converter. To get rid of this warning, go to the file\n     that contains the ' + name + ' class, look for the UIUtils.register call\n     and add a \'' + event.type + '\' property to the \'events\' object.';
};

function warning(text) {
  console.warn(DEBUG_SCOPE, text);
}

module.exports = {
  type: function type(_ref) {
    var component = _ref.component;

    var name = component.constructor.name;
    warning(getTypeNotSpecifiedText(name));
    return lowerCase(name);
  },
  label: function label(_ref2) {
    var component = _ref2.component;

    return component.props.labelTitle || component.props.label || component.props.id;
  },


  events: {
    click: {
      verb: 'clicked'
    },
    changed: {
      verb: 'changed'
    },
    contextmenu: {
      verb: 'right-clicked'
    },
    default: {
      verb: function verb(_ref3) {
        var event = _ref3.event;
        var component = _ref3.component;

        var name = component.constructor.name;
        warning(getEventNotMappedText(event, name));
        return event.type + 'ed';
      }
    }
  },

  preposition: 'in'
};