'use strict';

var lowerCase = require('lodash/lowerCase');
var warnings = require('../../warnings');
var RemediationUtils = require('../../RemediationUtils');

function red(str) {
  warnings.labelNotFound(str);
  return RemediationUtils.red(str);
}

module.exports = {
  type: function type(action) {
    var type = action.type;
    if (type) {
      return type;
    }

    var component = action.component;

    var name = component.constructor.name;
    warnings.typeNotSpecified(name);
    type = lowerCase(name);

    return type;
  },
  label: function label(action) {
    var label = action.label;

    // Check if the action supplies a label
    if (label) {
      return label;
    }

    var component = action.component;
    var props = component.props;

    if (!props) {
      return;
    }

    label = props.labelTitle || props.label || red(props.id);

    return label;
  },


  events: {
    click: {
      verb: 'clicked'
    },
    changed: {
      verb: 'changed'
    },
    contextmenu: {
      verb: 'right-clicked'
    },
    default: {
      verb: function verb(action) {
        var verb = action.verb;

        if (verb) {
          return verb;
        }

        var event = action.event;
        var component = action.component;

        var name = component.constructor.name;
        warnings.eventNotMapped(event, name);
        verb = event.type + 'ed';

        return verb;
      }
    }
  },

  preposition: 'in'
};