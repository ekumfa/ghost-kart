'use strict';

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var assign = require('lodash/assign');
var pako = require('pako');

var JSONSerializer = function () {
  function JSONSerializer() {
    var actions = arguments.length <= 0 || arguments[0] === undefined ? [] : arguments[0];
    var config = arguments.length <= 1 || arguments[1] === undefined ? {} : arguments[1];

    _classCallCheck(this, JSONSerializer);

    this.actions = actions;
    this.config = config;
  }

  _createClass(JSONSerializer, [{
    key: 'serialize',
    value: function serialize() {
      var output = this.actions.map(function (action) {
        var evt = assign({
          _meta: {
            dom: action.dom
          }
        }, action.event);
        return evt;
      });

      if (this.config.compress !== false) {
        output = pako.deflate(JSON.stringify(output), { to: 'string' });
      }

      return output;
    }
  }]);

  return JSONSerializer;
}();

module.exports = JSONSerializer;