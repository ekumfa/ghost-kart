'use strict';

var DEBUG_SCOPE = '[ghostKart]';

var isDisabled = false;

function warning(text) {
  if (!isDisabled) {
    console.warn(DEBUG_SCOPE, text);
  }
}

module.exports = {

  set isDisabled(val) {
    isDisabled = val;
  },

  actionMissingImportantParts: function actionMissingImportantParts(action) {
    warning('The reported action was missing a component and did not have skipMapping set.\n       Please see the following action data for specifics.');
    warning(action);
  },
  componentNotRegistered: function componentNotRegistered(name) {
    warning(name + ' was not registered. Using the default mapping.\n       To get rid of this warning, go to the file that contains the\n       ' + name + ' class, and add a UIUtils.register call.');
  },
  typeNotSpecified: function typeNotSpecified(name) {
    warning('Component type not given for ' + name + '. Using default type converter.\n       To get rid of this warning, go to the file that contains the ' + name + '\n       class, look for the UIUtils.register call and add a \'type\' property.');
  },
  labelNotFound: function labelNotFound(name) {
    warning('Component label not given for ' + name + '. Using default type converter.\n       To get rid of this warning, you may need to find the data for the control\n       with an ID of ' + name + ' or find the component class, look for the\n       UIUtils.register call and add/change the \'label\' function.');
  },
  eventNotMapped: function eventNotMapped(event, name) {
    warning('There was no default event mapper for the ' + event.type + '. Using\n       default event converter. To get rid of this warning, go to the file\n       that contains the ' + name + ' class, look for the UIUtils.register call\n       and add a \'' + event.type + '\' property to the \'events\' object.');
  }
};