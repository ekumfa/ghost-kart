'use strict'

let path = require('path')
let _ = require('lodash')
let ForceCaseSensitivityPlugin = require('force-case-sensitivity-webpack-plugin')

let outputPath = path.join(__dirname, 'dist', process.env.NODE_ENV || 'development')

let config = {
  entry: {
    index: './src/index.js'
  },

  output: {
    path: outputPath,
    filename: '[name].js',
    chunkFilename: '[name].js',
    sourceMapFilename: '[file].map'
  },

  module: {
    loaders: [
      {
        test: /\.(js|jsx)$/,
        exclude: /node_modules/,
        loader: 'babel!eslint?quiet=true'
      }
    ]
  },

  resolve: {
    extensions: ['', '.webpack.js', '.web.js', '.js', '.jsx']
  },

  plugins: [
    new ForceCaseSensitivityPlugin()
  ],

  devtool: 'source-map'
}

module.exports = config
