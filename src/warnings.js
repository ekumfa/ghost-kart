const DEBUG_SCOPE = '[ghostKart]'

let isDisabled = false

function warning(text) {
  if (!isDisabled) {
    console.warn(DEBUG_SCOPE, text)
  }
}

module.exports = {

  set isDisabled(val) {
    isDisabled = val
  },

  actionMissingImportantParts(action) {
    warning(
      `The reported action was missing a component and did not have skipMapping set.
       Please see the following action data for specifics.`
    )
    warning(action)
  },

  componentNotRegistered(name) {
    warning(
      `${name} was not registered. Using the default mapping.
       To get rid of this warning, go to the file that contains the
       ${name} class, and add a UIUtils.register call.`
    )
  },

  typeNotSpecified(name) {
    warning(
      `Component type not given for ${name}. Using default type converter.
       To get rid of this warning, go to the file that contains the ${name}
       class, look for the UIUtils.register call and add a 'type' property.`
    )
  },

  labelNotFound(name) {
    warning(
      `Component label not given for ${name}. Using default type converter.
       To get rid of this warning, you may need to find the data for the control
       with an ID of ${name} or find the component class, look for the
       UIUtils.register call and add/change the 'label' function.`
    )
  },

  eventNotMapped(event, name) {
    warning(
      `There was no default event mapper for the ${event.type}. Using
       default event converter. To get rid of this warning, go to the file
       that contains the ${name} class, look for the UIUtils.register call
       and add a '${event.type}' property to the 'events' object.`
    )
  }
}