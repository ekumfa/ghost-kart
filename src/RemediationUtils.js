/* eslint no-undefined: 0 */
'use strict'

const includes = require('lodash/includes')

const IGNORED_EVENT_PROPS = [
  'applicationCache',
  'bubbles',
  'cancelable',
  'clientInformation',
  'chrome',
  'console',
  'crypto',
  'defaultPrevented',
  'dispatchConfig',
  'dispatchMarker',
  '_dispatchIDs',
  '_dispatchInstances',
  '_dispatchListeners',
  '_targetInst',
  'external',
  'frames',
  'isTrusted',
  'nativeEvent',
  'navigator',
  'path',
  'parent',
  'performance',
  'runtime',
  'screen',
  'self',
  'sourceCapabilities',
  'styleMedia',
  'target',
  'top',
  'view',
  'webkitIndexedDB',
  'webstore',
  'window'
]

let RemediationUtils

function processProp(config, evt) {
  return (key, val) => {
    let retVal

    switch (true) {
      case (includes(IGNORED_EVENT_PROPS, key)):
        retVal = undefined
        break
      case (typeof val === 'function'):
        retVal = undefined
        break
      case (val instanceof Window):
        retVal = 'window'
        break
      case (val instanceof HTMLElement):
        retVal = RemediationUtils.getDOMSelector(val, config.root, evt)
        break
      default:
        retVal = val
    }

    return retVal
  }
}

function processEvent(evt, func) {
  const out = {}
  for (var key in evt) { // eslint-disable-line
    const val = func(key, evt[key])
    if (val !== undefined) {
      out[key] = val
    }
  }
  return out
}

function processSyntheticEvent(evt, func) {
  return JSON.parse(JSON.stringify(evt, func))
}

RemediationUtils = {

  red(str) {
    return `<span style="color: red;" title="Proper label not found">${str}</span>`
  },

  getAncestorRealms(component) {
    let realms = []
    while (component && component.context && component.context.realm) {
      realms.push(component = component.context.realm)
    }
    return realms.reverse()
  },

  /*
   * config = {
   *   root: [The root simulation ReactElement]
   * }
   */
  convertEvent(evt, config = {}) {
    const func = processProp(config, evt)
    let out
    if (evt instanceof Event) {
      out = processEvent(evt, func)
    } else {
      out = processSyntheticEvent(evt, func)
    }
    return out
  },

  getDOMSelector(node, rootEl, evt) {
    const selector = []
    const elId = evt.target.id
    const setId = (id) => {
      const els = document.querySelectorAll(`#${id}`) || []
      if (els.length > 1) {
        console.warn('ID: ', id, 'is used on mutliple Elements. ', els)
      } else {
        selector.unshift(id)
      }
    }

    if (typeof elId === 'undefined' || elId === '') {
      let currNode = node
      let parentNode = evt.target.parentNode
      let shouldContinue = false
      while (parentNode.id === '' || currNode === node || shouldContinue) {
        const childNodes = parentNode && parentNode.childNodes
        const childIndex = childNodes && Array.from(childNodes).indexOf(currNode)
        if (childIndex === -1) {
          break
        }

        selector.unshift(childIndex)

        if (parentNode.id) {
          setId(parentNode.id)
          break
        }

        currNode = parentNode
        parentNode = parentNode.parentNode
        if (parentNode.id) {
          shouldContinue = true // Run loop one more time
        }
      }
    } else {
      setId(elId)
    }

    return selector.join('.')
  }
}

module.exports = RemediationUtils
