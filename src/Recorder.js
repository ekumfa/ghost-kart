'use strict'

let uuid = require('node-uuid')
let isFunction = require('lodash/isFunction')
let defaultsDeep = require('lodash/defaultsDeep')
let RemediationUtils = require('./RemediationUtils')
let {findDOMNode} = require('react-dom')
let warnings = require('./warnings')

class Recorder {

  constructor(config = {}) {
    this.id = uuid.v1()
    this.rootEl = config.rootEl
    this.actions = []
    this.isRecording = false
    this.config = config
  }

  get sequence() {
    return this.actions
  }

  /**
   * This method adds a user interaction to the record.
   * @param {object} data Arbitrary metadata that describes the event
   * @returns {void}
   */
  addAction(data) {
    let rootDomEl = findDOMNode(this.rootEl)

    if (!data.component && typeof data.skipMapping === 'undefined') {
      warnings.actionMissingImportantParts(data)
    }

    data = defaultsDeep(data || {}, {
      type: '',
      event: {},
      component: {},
      meta: {},
      dom: {
        id: rootDomEl.id,
        width: rootDomEl.offsetWidth,
        height: rootDomEl.offsetHeight
      }
    })

    // Do this now because React's SyntheticEvent is reused.
    data.event = RemediationUtils.convertEvent(data.event, {
      root: this.rootEl
    })

    let insMode = data.insertion || 'append'
    const currLen = this.actions.length

    if (isFunction(insMode)) {
      insMode = insMode(this.actions[currLen - 1], data)
    }

    switch (insMode) {
      case 'negate':
        // remove last action
        this.actions.splice(-1, 1)
        break
      case 'replace':
        // remove last action and replace with this one
        this.actions.splice(-1, 1, data)
        break
      default:
        // append current action
        this.actions.push(data)
    }
  }

  pause() {
    this.isRecording = false
  }

  record() {
    this.isRecording = true
  }
}

module.exports = Recorder
