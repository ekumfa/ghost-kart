'use strict'

let uuid = require('node-uuid')
let isEmpty = require('lodash/isEmpty')
let {Simulate} = require('react-dom/test-utils')

const EventMap = require('./playback/DOMToReactTypeMap')

const DEFAULT_ROOT_EL_ID = 'ArubaSimulation'
const PLAY_MIN_SPEED = 200
const DEFAULT_ACTION_DELAY = 10

function generateError(action, msg) {
  const err = new Error(msg)
  err.details = {action}
  return err
}

class Player {
  constructor(actions = [], config = {}) {
    this.actions = actions
    this.config = config
    this.currentIndex = 0
  }

  playCurrentAction(done) {
    let action = this.actions[this.currentIndex]
    const genErr = generateError.bind(null, action)

    if (action._meta) { // eslint-disable-line no-underscore-dangle
      let data = action._meta.dom // eslint-disable-line no-underscore-dangle
      let rootEl = document.getElementById(data.id || DEFAULT_ROOT_EL_ID)
      rootEl.style.width = `${data.width}px`
      rootEl.style.height = `${data.height}px`
      try {
        let ev = new Event('resize')
        window.dispatchEvent(ev)
      } catch (err) {
        console.info(err)
        Simulate.resize(window)
      }

      delete action._meta // eslint-disable-line no-underscore-dangle
    }

    if (!this.config.skipAnimation) {
      // 2. Move mouse to correct location
      // 3. Play sound
    }

    // 4. Fire synthetic event
    let el
    let type = action.type
    let domSel = action.currentTarget
    if (domSel.includes('.')) {
      const [parentId, ...childIndices] = domSel.split('.')
      el = document.getElementById(parentId)
      if (el) {
        childIndices.forEach(idx => {
          el = el && el.childNodes[idx]
        })
      }
    } else {
      el = document.querySelector(domSel)
    }

    if (!el) {
      return done(genErr(`Could not find element based on given selector: ${domSel}`))
    }

    setTimeout(() => {
      const simulatedEvent = Simulate[type] || Simulate[EventMap[type]]
      if (simulatedEvent) {
        simulatedEvent(el, {type})
        done()
      } else {
        done(genErr(`Could not recreate event of type '${type}'`))
      }
    }, this.config.actionDelay || DEFAULT_ACTION_DELAY)
  }

  play() {
    let actions = this.actions || []

    if (!isEmpty(actions)) {

      this.playCurrentAction((err) => {

        if (err) {
          if (this.config.onComplete) {
            this.config.onComplete(err)
          }
          return
        }

        this.currentIndex = this.currentIndex + 1
        if (!this.isPaused && this.currentIndex < actions.length) {
          this.play()
        } else if (this.currentIndex === actions.length) {
          this.isPaused = true
          this.isComplete = true
          if (this.config.onComplete) {
            this.config.onComplete()
          }
        }
      })
    }
  }
}

class Playback {
  constructor(actions = [], config = {}) {
    this.id = uuid.v1()
    this.actions = actions
    this.config = config
    this.isStarted = false
    this.isPlaying = false
    this.player = new Player(this.actions, this.config)
  }

  get isPaused() {
    return this.isStarted && !this.isPlaying
  }

  start() {
    if (!this.isStarted) {
      this.isStarted = true
      this.player.currentIndex = 0
    }
    this.resume()
  }

  pause() {
    this.isPlaying = false
    this.player.isPaused = true
  }

  resume() {
    if (!this.isPlaying) {
      this.isPlaying = true
      this.player.isPaused = false
      this.player.play()
    }
  }
}

module.exports = Playback
