'use strict'

let lowerCase = require('lodash/lowerCase')
let warnings = require('../../warnings')
let RemediationUtils = require('../../RemediationUtils')

function red(str) {
  warnings.labelNotFound(str)
  return RemediationUtils.red(str)
}

module.exports = {
  type(action) {
    let type = action.type
    if (type) {
      return type
    }

    let {component} = action
    let name = component.constructor.name
    warnings.typeNotSpecified(name)
    type = lowerCase(name)

    return type
  },

  label(action) {
    let label = action.label

    // Check if the action supplies a label
    if (label) {
      return label
    }

    let component = action.component
    let props = component.props

    if (!props) {
      return
    }

    label = props.labelTitle || props.label || red(props.id)

    return label
  },

  events: {
    click: {
      verb: 'clicked'
    },
    changed: {
      verb: 'changed'
    },
    contextmenu: {
      verb: 'right-clicked'
    },
    default: {
      verb(action) {
        let verb = action.verb

        if (verb) {
          return verb
        }

        let {event, component} = action
        let name = component.constructor.name
        warnings.eventNotMapped(event, name)
        verb = event.type + 'ed'

        return verb
      }
    }
  },

  preposition: 'in'
}
