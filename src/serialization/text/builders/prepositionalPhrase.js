let buildDirectObject = require('./directObject')

module.exports = function (action, getMapper) {
  let mapper = getMapper(action)
  let prep = mapper.preposition
  let complement = buildDirectObject(action, mapper, 'i')
  return `${prep} ${complement}`
}
