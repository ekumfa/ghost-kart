let upperFirst = require('lodash/upperFirst')
let sentence = require('./sentence')

module.exports = function (groups, getMapper, useConnectors) {
  let sentences = groups.map((group) => {
    return sentence(group, getMapper)
  })

  if (sentences.length === 0) {
    return ''
  }

  return sentences.map((text, index) => {
    if (useConnectors) {
      if (sentences.length > 2 && index === sentences.length - 1) {
        text = 'finally, ' + text
      } else if (index > 0) {
        text = 'then, ' + text
      }
    }
    return upperFirst(text)
  }).join('. ') + '.'
}
