let directObject = require('./directObject')
let paragraph = require('./paragraph')
let predicate = require('./predicate')
let prepositionalPhrase = require('./prepositionalPhrase')
let sentence = require('./sentence')

module.exports = {
  directObject, // Also used for prepositional complement
  paragraph,
  predicate,
  prepositionalPhrase,
  sentence
}
