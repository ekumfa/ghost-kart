let prepPhrase = require('./prepositionalPhrase')
let predicate = require('./predicate')

const NAME_STR = 'you'
const UPPER_FIRST_NAME_STR = 'You'

const NAME_TAG = '<span class="username">you</span>'

function getNameTag(str) {
  return `<span class="username">${str}</span>`
}

module.exports = function (parts, getMapper) {
  let fragments = []
  let realms

  if (parts.length === 0) {
    return fragments
  }

  // The realms for each part should be the same,
  // so we just neead to take the first set
  realms = parts[0].realms
  if (realms.length !== 0) {
    // there are prepositions to build
    fragments.push(realms.map((cmp) => {
      return prepPhrase({component: cmp}, getMapper)
    }).join(' '))

  }

  fragments = fragments.concat(parts.map((action, index) => {
    let output = predicate(action, getMapper)
    if (index === 0) {
      output = getNameTag(fragments.length === 0 ? UPPER_FIRST_NAME_STR : NAME_STR) + ' ' + output
    } else if (parts.length > 2 && index === parts.length - 1) {
      output = 'and ' + output
    }
    return output
  }))

  return fragments.join(', ')
}
