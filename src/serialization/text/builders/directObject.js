let isFunction = require('lodash/isFunction')

// Also used for prepositional complement
module.exports = function (action, mapper, tag = 'b') {
  let type = mapper.type
  let label = mapper.label

  if (isFunction(type)) {
    type = type(action)
  }

  if (!label) {
    return `a ${type}`
  }

  if (isFunction(label)) {
    label = label(action)
  }
  return `the <${tag}>${label}</${tag}> ${type}`
}
