let isFunction = require('lodash/isFunction')
let isString = require('lodash/isString')
let buildDirectObject = require('./directObject')

module.exports = function (action, getMapper) {
  let predicate = ''
  let {event} = action
  let mapper = getMapper(action)
  let eventMapper = mapper.events[event.type]

  if (!eventMapper) {
    eventMapper = mapper.events.default
  }

  if (isString(eventMapper)) {
    predicate = eventMapper
  } else if (isFunction(eventMapper)) {
    predicate = eventMapper(action)
  } else if (eventMapper.verb) {
    let verb = eventMapper.verb
    if (isFunction(verb)) {
      verb = verb(action)
    }
    let directObject = buildDirectObject(action, mapper)

    predicate = `${verb} ${directObject}`
  }

  return predicate
}