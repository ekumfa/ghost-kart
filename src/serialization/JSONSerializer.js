'use strict'

const assign = require('lodash/assign')
const pako = require('pako')

class JSONSerializer {

  constructor(actions = [], config = {}) {
    this.actions = actions
    this.config = config
  }

  serialize() {
    let output = this.actions.map((action) => {
      let evt = assign({
        _meta: {
          dom: action.dom
        }
      }, action.event)
      return evt
    })

    if (this.config.compress !== false) {
      output = pako.deflate(JSON.stringify(output), {to: 'string'})
    }

    return output
  }


}

module.exports = JSONSerializer
