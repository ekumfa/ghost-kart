'use strict'

let cloneDeep = require('lodash/cloneDeep')
let defaultsDeep = require('lodash/defaultsDeep')
let difference = require('lodash/difference')

const DEFAULT_SOFT_LIMIT = 500
const DEFAULT_THRESHOLD = 0.15 // percent

const DEFAULT_MAPPINGS = require('./text/DefaultMappings')
let BUILDERS = require('./text/builders')

let warnings = require('../warnings')
let RemediationUtils = require('../RemediationUtils')

let map = new WeakMap()

function getMapper(action) {
  let defMapClone = cloneDeep(TextSerializer.DEFAULT_MAPPINGS) // eslint-disable-line
  if (action.skipMapping) {
    return defMapClone
  }

  let component = action.component
  let constr = component.constructor
  let mapper = map.get(constr)

  if (!mapper) {
    warnings.componentNotRegistered(constr.name)
    mapper = defMapClone
  }

  return mapper
}

function groupActionsByRealms(actions) {
  let groups = []
  let currentGroup

  actions.forEach((action) => {
    let {component} = action
    action.realms = RemediationUtils.getAncestorRealms(component)
  })

  function createNewGroupWithAction(action) {
    currentGroup = [action]
    groups.push(currentGroup)
  }

  actions.forEach((action, idx) => {
    let lastAction

    if (idx === 0) {
      // first group
      createNewGroupWithAction(action)
    } else {
      lastAction = actions[idx - 1]

      if (lastAction.realms.length !== action.realms.length) {
        // new group
        createNewGroupWithAction(action)
      } else if (difference(lastAction.realms, action.realms).length > 0) {
        // new group
        createNewGroupWithAction(action)
      } else {
        currentGroup.push(action)
      }
    }
  })

  return groups
}

function truncateText(text, limit) {
  if (limit === -1 || text.length < limit) {
    return text
  }

  const lines = text.split('. ')
    .map((line) => line.trim())
    .filter((line) => (line.length > 0))

  let truncatedText = ''
  let nextText = ''

  for (let i = lines.length; i > 0; i = i - 1) {
    nextText = lines.slice(i - 1).join('. ')
    if (nextText.length < limit) {
      truncatedText = nextText
    } else {
      break
    }
  }

  return truncatedText
}

class TextSerializer {

  static registerComponent(cmpClass, meta = {}) {
    defaultsDeep(meta, TextSerializer.DEFAULT_MAPPINGS)
    map.set(cmpClass, meta)
  }

  static DEFAULT_MAPPINGS = DEFAULT_MAPPINGS

  constructor(actions = [], config = {}) {
    actions = actions.filter((act) => {
      return (act.skipText !== true)
    })
    this.actions = actions
    this.config = config
  }

  serialize() {
    const groups = groupActionsByRealms(this.actions)
    let paragraph = BUILDERS.paragraph(groups, getMapper, this.config.useConnectors || false)

    let softLimit = this.config.softLimit
    if (typeof softLimit === 'undefined') {
      softLimit = DEFAULT_SOFT_LIMIT
    }
    let hardLimit = softLimit === -1 ? -1 : Math.floor(softLimit * (1 + DEFAULT_THRESHOLD))

    return truncateText(paragraph, hardLimit)
  }
}

module.exports = TextSerializer
