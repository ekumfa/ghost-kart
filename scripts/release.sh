#!/usr/bin/env bash

npm version $1 && npm publish && \
git checkout master && git merge develop && \
git push && git checkout develop && git push
