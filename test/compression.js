'use strict'

const fs = require('fs')
const path = require('path')
const readline = require('readline')
const MongoClient = require('mongodb').MongoClient
const pako = require('pako')
const sizeof = require('object-sizeof')

const REPORT_FILE = 'report.csv'
const DB_URL = process.env.DB_URL
// const SAMPLE_SIZE = 100

const reportOutput = []
let attemptsCount = 0
let attemptsReceived = 0

function handleError(db) {
  return function (err) {
    console.error(err)
    if (db) {
      db.close()
    }
  }
}

function repeatChar(count, ch) {
  let output
  if (count === 0) {
    output = ''
  } else {
    const count2 = count / 2
    let result = ch

    // double the input until it is long enough.
    while (result.length <= count2) {
      result += result
    }
    // use substring to hit the precise length target without
    // using extra memory
    output = result + result.substring(0, count - result.length)
  }
  return output
}

function printRemainingItems() {
  // readline.clearLine(process.stdout, 0)
  // readline.cursorTo(process.stdout, 0)

  const cols = process.stdout.columns
  const width = Math.floor(cols * attemptsReceived / attemptsCount)

  let output = ''

  if (width > 0) {
    output += '\u2588'
  }

  readline.cursorTo(process.stdout, width - 1)
  process.stdout.write(output, {encoding: 'utf8'})
}

function writeDataToFile() {
  const filepath = path.join(__dirname, '../report.csv')
  const content = reportOutput.join('')
  fs.writeFileSync(filepath, content)
}

function processAttempt(att) {
  const json = att.json
  const raw = JSON.stringify(json)
  const rawSize = sizeof(raw)
  const deflated = pako.deflate(raw, {to: 'string'})
  const defSize = sizeof(deflated)
  const ratio = defSize / rawSize * 100.0
  reportOutput.push(`${rawSize},${defSize},${ratio.toFixed(2)}\n`)
}

function getRandomSample(db) {
  const stream = db.collection('attempts')
    .find({})
    .stream()

  stream.on('end', () => {
    db.close()
    readline.clearLine(process.stdout, 0)
    process.stdout.write('Done\n')
    writeDataToFile()
  })

  stream.on('data', (data) => {
    attemptsReceived += 1
    printRemainingItems()
    if (typeof data === 'object' && data.json) {
      processAttempt(data)
    }
  })
}

function getCount(db) {
  db.collection('attempts')
    .count()
    .catch(handleError)
    .then((count) => {
      attemptsCount = count
      printRemainingItems()
      getRandomSample(db)
    })
}

process.stdout.write('\x1B[?25l')
MongoClient.connect(DB_URL)
  .catch(handleError)
  .then(getCount)
